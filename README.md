# THIS REPO IS DEPRECATED
This program is no longer needed for dwm as `dwmstatusbar` effectively replaces this. Any dwm status scripts from [my dotfiles repo](https://gitlab.com/fawzakin/dotfiles) are moved here.

# dwmblocks
![screenshot of dwmblocks](https://gitlab.com/fawzakin/dwmblocks/raw/main/demo.png)  
dwmblocks is a simple program to output usefull information to dwm statusbar.  
It is so simple that I only need to edit the blocks.def.h. I don't need to modify this program any further so it's better to just fork it from someone else's repo and have my own blocks.def.h.

# License and Credit
GNU GPLv2.
